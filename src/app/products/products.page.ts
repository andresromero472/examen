import { Component, OnInit } from '@angular/core';
import { producto } from './products.model';
import { ServiceService } from './service.service';
import { BehaviorSubject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {
  productos: producto [];
  textoBuscar = '';
  carrito = [];

  constructor(private servicio: ServiceService) {}



  ngOnInit() {
    
  }
  ionViewWillEnter(){
    this.productos = this.servicio.GetAllProducts();
  }

  
  buscar(event){
    //console.log(event);
    this.textoBuscar = event.detail.value;
  }
}
