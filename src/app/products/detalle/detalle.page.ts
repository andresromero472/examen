import { Component, OnInit } from '@angular/core';
import { producto } from '../products.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from '../service.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {
  productoActual: producto;
  constructor(
    private route: ActivatedRoute,
    private servicio: ServiceService,
    private router: Router,
    private alert: AlertController
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap =>{
      if(!paramMap.has('productID')){
        return;
      }
      const productoId = parseInt(paramMap.get('productID'));
      this.productoActual = this.servicio.GetProduct(productoId);
    });
  }
  async deleteProduct() {
    const alert = await this.alert.create({
      cssClass: 'my-custom-class',
      header: '¿Está seguro que desea eliminar?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'SI',
          handler: () => {
            this.servicio.deleteProduct((this.productoActual.id));
            this.router.navigate(['/products']);
          }
        }
      ]
    });

    await alert.present();
  }
}
