export interface producto{
    id: number;
    producto: string;
    descripcion: string;
    image: string;
    precio: number;
}