import { Injectable } from '@angular/core';
import { producto } from './products.model';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  push(Number: NumberConstructor) {
    throw new Error("Method not implemented.");
  }
  private productos: producto [] = [
    {
      id: 1,
      producto: 'Scott Scale 910',
      descripcion: 'Scale 910 tiene una geometría inspirada en las carreras al más alto nivel, un tren de transmisión Shimano XT de 12 velocidades y componentes Syncros se combinan para brindarte una cola dura que solo está ansiosa por terminar primero.' ,
      image: 'assets/media/products/scale910.jpg' ,
      precio: 2200000,
    },
    {
      id: 2,
      producto: 'Maxis Ikon',
      descripcion: 'El Ikon es para verdaderos corredores que buscan un verdadero neumático de carrera ligero. Con tecnología 3C Triple Compound, carcasa de alto volumen y un diseño de banda de rodadura rápida, el Ikon ofrece un rendimiento ejemplar' ,
      image: 'assets/media/products/maxisikon.png' ,
      precio: 35000,
    },    
    {
      id: 3,
      producto: 'Mountain King ProTection',
      descripcion: 'El compañero de ruta perfecto: no es tacaño en el agarre, ideal para bicicletas de trail con suspensión total.Las perillas centrales ágiles y las perillas exteriores con agarre significan que ofrece un agarre máximo.',
      image: 'assets/media/products/moutainkingpro.png' ,
      precio: 38000,
    },    
    {
      id: 4,
      producto: 'Specialized S-Works Epic AXS',
      descripcion: 'Si sigues las carreras de Cross Country, sin duda habrás notado que la Epic ocupa el primer lugar más veces de las que nadie puede contar, incluidos títulos del Campeonato Mundial, medallas olímpicas y Cape Epic.' ,
      image: 'assets/media/products/epic2020.jpg' ,
      precio: 4500000,
    },    
    {
      id: 5,
      producto: 'Trek Supercaliber 9.9',
      descripcion: 'Supercaliber 9.9 es la bicicleta de carrera de cross country más rápida, ligera y capaz que fabricamos, y esa es exactamente la razón por la cual es la bicicleta de la Copa Mundial de Trek Factory Racing. El exclusivo amortiguador de tubo superior IsoStrut es la piedra angular del eficiente sistema de suspensión de Supercaliber, que le brinda una conducción rápida y ligera que se carga a través de terreno accidentado a una velocidad vertiginosa.' ,
      image: 'assets/media/products/supercaliber.jpg' ,
      precio: 5850000,
    },    
    {
      id: 6,
      producto: 'Edge® 1030',
      descripcion: 'Computadora para bicicleta de 3.5 "con funciones completas de navegación, rendimiento y conciencia de ciclismo',
      image: 'assets/media/products/1030.jpg' ,
      precio: 375000,
    },    
    {
      id: 7,
      producto: 'Syncros Fraser IC SL Carbon Handlebar',
      descripcion: 'Nuestro combo de manillar / vástago de cross country de una pieza es lo último en peso ligero para el exigente piloto de XC. Basado en nuestro popular manillar plano y un vástago de -8 ° en 3 opciones de extensión virtual diferentes, nuestros ingenieros de carbono han podido ahorrar un peso considerable con el mismo rendimiento. Esta cabina es utilizada por Kate Courtney en el circuito de la Copa del Mundo.' ,
      image: 'assets/media/products/syncrossfrasericslcarbon.jpg' ,
      precio: 275000,
    },    
    {
      id: 8,
      producto: 'ELEMNT BOLT GPS BIKE COMPUTER',
      descripcion: '¡ELEMNT BOLT es la primera computadora para bicicleta GPS completamente aerodinámica! Su diseño pendiente de patente crea un sistema integrado de computadora y soporte que da como resultado un sistema CFD (Computational Fluid Dynamics) altamente aerodinámico diseñado para estar en la parte delantera de su bicicleta. Equipado con Bluetooth Smart y tecnología de banda dual ANT +, ELEMNT BOLT se combina perfectamente con todos sus sensores de ciclismo.' ,
      image: 'assets/media/products/elemnt-bolt-gps-bike-computer-1.jpg' ,
      precio: 170000,
    },    
    {
      id: 9,
      producto: 'SRAM X01 Eagle AXS',
      descripcion: 'Hay un elemento de lo desconocido cuando se trata de correr un nuevo sendero y correr a ciegas. Las especificaciones de enduro siguen su ejemplo. X01 Eagle AXS utiliza la red troncal probada de SRAM 1x. Rango de engranaje del 500 por ciento. Plato X-SYNC 2 más seguro y duradero. La resistencia de una jaula de aluminio, junto con los componentes conectados de Eagle AXS. Listo para la nueva generación de súper motos.' ,
      image: 'assets/media/products/blacksidel.jpg' ,
      precio: 1150000 ,
    },
    {
      id: 10,
      producto: 'Shimano XTR GroupSet',
      descripcion: 'En el ciclismo de montaña, las condiciones del sendero cambian drásticamente de un segundo a otro, de un giro al siguiente. Los ciclistas de montaña deben ser capaces de percibir sin problemas las condiciones, juzgar sus opciones y operar su bicicleta en una fracción de segundo. XTR satisface las necesidades de los ciclistas más exigentes al proporcionar el más alto nivel de precisión e integración. Nos reenfocamos en el objetivo original de XTR: ser los mejores componentes de la carrera' ,
      image: 'assets/media/products/xtr.jpg' ,
      precio: 1200000 ,
    }
  ]
  constructor() {} 
  GetAllProducts(){
    return [...this.productos];
  }
  
  GetProduct(productoId: number){
    return {
      ...this.productos.find(producto => {
        return productoId === producto.id;
      })
    };
  }
  deleteProduct(productoID: number){
    return {
        ... this.productos = this.productos.filter(producto => {
        return producto.id !== productoID;
      })
    };
  }
  //Metodos de Carrito Compras
  addCart(productoId: number){
    this.carrito.push(productoId);
  }
  GetAllCarrito(){ 
    return [...this.carrito];
  }
  clearCart() {
    this.carrito = [];
    return this.carrito;
  }
  private carrito =[];
}
